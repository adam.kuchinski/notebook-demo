# Databricks notebook source
# Create a sample Dataframe with duplicate keys
# The dataset key is id in this example
# We only want to keep records with the max sequence number per key
df = spark.createDataFrame([
      ("a", 1234, 10, 20, 30),
      ("a", 1236, 15, 20, 30),
      ("b", 1238, 20, 20, 30),
      ("c", 1240, 30, 20, 30),
      ("c", 1241, 40, 20, 30)
]).toDF("id","sequence","value1", "value2", "value3")
# Now dedup with max_by
df.createOrReplaceTempView("df")



# COMMAND ----------

sql = """
  select id, maxRecStruct.*
  from (
  select id, max_by(struct1, sequence) as maxRecStruct
  from (select id, sequence, struct(sequence, value1, value2, value3) as struct1 from df)
  group by id
  )
  """


# COMMAND ----------

df2 = spark.sql(sql)
display(df2)

# COMMAND ----------

