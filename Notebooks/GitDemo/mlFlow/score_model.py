# Databricks notebook source
# MAGIC %md
# MAGIC 
# MAGIC ## Let's make an edit for Git!!
# MAGIC 
# MAGIC ### How about another edit?!

# COMMAND ----------

# MAGIC %sh curl -X POST -H "Content-Type:application/json; format=pandas-split" --data '{"columns":["age","workclass","fnlwgt","education","education_num","marital_status","occupation","relationship","race","sex","capital_gain","capital_loss","hours_per_week","native_country"],"index":[0],"data":[[39.0," State-gov",77516.0," Bachelors",13.0," Never-married"," Adm-clerical"," Not-in-family"," White"," Male",2174.0,0.0,40.0," United-States"]]}' http://localhost:8080/invocations

# COMMAND ----------

from pyspark.sql.types import DoubleType, StringType, StructType, StructField

schema = StructType([
  StructField("age", DoubleType(), False),
  StructField("workclass", StringType(), False),
  StructField("fnlwgt", DoubleType(), False),
  StructField("education", StringType(), False),
  StructField("education_num", DoubleType(), False),
  StructField("marital_status", StringType(), False),
  StructField("occupation", StringType(), False),
  StructField("relationship", StringType(), False),
  StructField("race", StringType(), False),
  StructField("sex", StringType(), False),
  StructField("capital_gain", DoubleType(), False),
  StructField("capital_loss", DoubleType(), False),
  StructField("hours_per_week", DoubleType(), False),
  StructField("native_country", StringType(), False),
  StructField("income", StringType(), False)
])
input_df = spark.read.format("csv").schema(schema).load("/databricks-datasets/adult/adult.data")

train_df, test_df = input_df.randomSplit([0.99, 0.01], seed=42)

# COMMAND ----------

import mlflow

model_uri = f"runs:/a94520f50e454026aba1a726fc4f3cfc/model"

# Prepare test dataset
test_pdf = test_df.toPandas()
y_test = test_pdf["income"]
X_test = test_pdf.drop("income", axis=1)

# Run inference using the best model
model = mlflow.pyfunc.load_model(model_uri)
predictions = model.predict(X_test)
test_pdf["income_predicted"] = predictions
display(test_pdf)

# COMMAND ----------

scoring_url = "http://localhost:8080/invocations"

# COMMAND ----------

import os
import requests
import pandas as pd

def score_model(data_json):
    headers = {"Authorization": f"Bearer {token}"}
    rsp = requests.request(method="POST", headers=headers, url=scoring_url, json=data_json)
    if rsp.status_code != 200:
        raise Exception(f"Request failed with status {rsp.status_code}, {rsp.text}")
    return rsp.json()