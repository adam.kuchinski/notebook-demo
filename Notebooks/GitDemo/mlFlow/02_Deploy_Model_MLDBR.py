# Databricks notebook source
# MAGIC %md ## Launch model server on driver node
# MAGIC * Uses the MLflow CLI command: `mlflow models serve`.
# MAGIC * Example: `mlflow models serve --model-uri models:/iris/1 --host 0.0.0.0 --port 54322`.
# MAGIC * MLflow documentation: [Deploy MLflow models](https://mlflow.org/docs/latest/models.html#deploy-mlflow-models).

# COMMAND ----------

# MAGIC %md ### Setup

# COMMAND ----------

# MAGIC %md ##### Create widgets

# COMMAND ----------

dbutils.widgets.text("Registered model","")
registered_model = dbutils.widgets.get("Registered model")

dbutils.widgets.text("Version", "1")
version = dbutils.widgets.get("Version")

if registered_model == "":
    raise Exception("Registered model required")

registered_model, version

# COMMAND ----------

# MAGIC %md ##### Create the Databricks credentials file needed for MLflow CLI

# COMMAND ----------

token = dbutils.notebook.entry_point.getDbutils().notebook().getContext().apiToken().get()
host_name = dbutils.notebook.entry_point.getDbutils().notebook().getContext().tags().get("browserHostName").get()
dbutils.fs.put("file:///root/.databrickscfg",f"[DEFAULT]\nhost=https://{host_name}\ntoken = "+token,overwrite=True)

# COMMAND ----------

# MAGIC %sh databricks clusters list

# COMMAND ----------

# MAGIC %md ##### Create model URI to deploy

# COMMAND ----------

#model_uri = f"models:/{registered_model}/{version}"
model_uri = f"runs:/a94520f50e454026aba1a726fc4f3cfc/model"
import os
os.environ["MODEL_URI"] = model_uri
model_uri

# COMMAND ----------

dbutils.widgets.text("Registered model","")
registered_model = dbutils.widgets.get("Registered model")

dbutils.widgets.text("Version", "1")
version = dbutils.widgets.get("Version")

if registered_model == "":
    raise Exception("Registered model required")

registered_model, version

# COMMAND ----------

# MAGIC %md ### Launch model server
# MAGIC 
# MAGIC Wait until you see the following message which indicates the server is ready to accept predictions. 
# MAGIC 
# MAGIC ```
# MAGIC [2020-03-21 21:31:51 +0000] [4512] [INFO] Starting gunicorn 20.0.4
# MAGIC [2020-03-21 21:31:51 +0000] [4512] [INFO] Listening at: http://0.0.0.0:54322 (4512)
# MAGIC [2020-03-21 21:31:51 +0000] [4512] [INFO] Using worker: sync
# MAGIC [2020-03-21 21:31:51 +0000] [4515] [INFO] Booting worker with pid: 4515
# MAGIC ```

# COMMAND ----------

# MAGIC %sh conda list

# COMMAND ----------

# MAGIC %sh conda list

# COMMAND ----------

# MAGIC %pip install mlflow[extras]

# COMMAND ----------

# MAGIC %pip install mlserver-sklearn

# COMMAND ----------

# MAGIC %sh mlflow models serve -m $MODEL_URI --enable-mlserver

# COMMAND ----------

# MAGIC %sh mlserver --help

# COMMAND ----------

# MAGIC %sh mlflow models build -m $MODEL_URI --enable-mlserver -n my-model222

# COMMAND ----------

# MAGIC %sh mlflow models serve --model-uri $MODEL_URI --host 0.0.0.0 --port 54322

# COMMAND ----------

# MAGIC %md ### Stop model server
# MAGIC 
# MAGIC To stop the model server you have to first cancel the above cell. The server will still be running, so execute the cells below. 

# COMMAND ----------

# MAGIC %sh ps -fae | grep mlflow.pyfunc.scoring | grep -v grep

# COMMAND ----------

# MAGIC %sh ps -fae | grep mlflow.pyfunc.scoring | grep -v grep | awk '{print $2}' |  xargs kill