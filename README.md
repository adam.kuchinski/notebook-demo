# notebook-demo



## Getting started

The best use of the Databricks CLI for Git versioning is to do a recursive directory download/upload. This allows you to version a group of notebooks at the directory level. When using this system, it's important that the directory in the Databricks workspace has an appropriate version attached.

The following might be a workflow for this strategy:

1. (Create and) checkout a branch for development. 
```
git checkout -b 'some-dir-0.0.1-dev'
```

2. Download your work from your Databricks Workspace
```
databricks workspace export_dir -o /Users/some.user@email.com/some-dir-0.0.1-dev .
```

3. Do a standard git commit of the work, e.g.
```
git add -A
git commit -m 'merging 0.0.1-dev to qa'
git push --set-upstream origin some-dir-0.0.1-dev
```

4. Merge and close the branch per your standard practice.

5. Checkout a new branch and upload to your workspace
```
git checkout some-dir-0.0.1-qa
databricks workspace import_dir -o . /Users/some.user@email.com/some-dir-0.0.1-qa
```

## Using scripts to make this simpler

Instead of repeating all of the steps above, we can use the export_commit.sample script to perform the following:
1. Check out the desired branch.
2. Pull new changes from the remote branch.
3. Export notebooks from the Databricks workspace using the Databricks CLI.
4. Prompt the user for a commit message or use the default if one is not provided.
5. Commit the updated notebooks to the local branch.
6. Push the changes to the remote branch.

Then you will still want to merge the branch per your standard practice.  

And we can use the pull_import.sample script to pull the latest code from git and upload it to your databricks workspace.


## Configure databricks-cli and create a new repository

You must have the databricks-cli installed and configured in an environment with Python 3.6+
  - [Instructions here](https://docs.databricks.com/dev-tools/cli/index.html)
  - If this is the first time that you are using the databricks-cli on this machine, be sure to complete the authentication with your workspace host and personal token
    - You can setup multiple profiles if you have more than one Databricks workspace
  - Test that the CLI is properly configured by running a command such as:
    - ` databricks clusters list`
    - or `databricks clusters list --profile <name of your profile from ~/.databrickscfg>`


## Setup your new repo

- [ ] copy the .gitignore, export_commit.sample, and pull_import.sample from this repo, commit and push them to your main branch
- [ ] Create a /Notebooks directory

## Setup your first branch

- [ ] It is recommended to create a different branch for each folder of notebooks that you want to add

```
cd existing_repo
git checkout -b branch_name
mkdir Notebooks/branch_name
```

- [ ] You could add additional folder structure within Notebooks/ if desired.  For example,  Notebooks/Demos/sql
- [ ] Copy the export_commit.sample into the new branch_name directory.  Edit the file by updating the < branch >, < profile >, < path >, and second < branch > placeholders.  Example:

```
git checkout sql
git pull 
databricks workspace export_dir --profile my_databricks_workspace -o /Users/adam.kuchinski@databricks.com/Demos/sql .


dt=`date '+%Y-%m-%d %H:%M:%S'`
msg_default="DB export on $dt"
read -p "Enter the commit comment [$msg_default]: " msg
msg=${msg:-$msg_default}
echo $msg

git add .
git commit -m $msg
git push --set-upstream origin sql
```

- [ ] Change the extension for this copy of the file to export_commit.sh
- [ ] Execute the script from within the branch directory.  

## Repeat for additional notebook folders

- [ ] Note that if you have notebooks in sub-folders, you can choose whether to create one export_commit script at the top level directory, or put one in each of the sub-directories.  


## Jobs

You can duplicate the jobs_submit.sample file and change the extension to .sh.  Then edit it to run a job that you have already created in Databricks.  Exmaple:

```
#!/bin/bash

source ~/opt/anaconda3/bin/activate dbricks

databricks jobs run-now --profile cs  --job-id 113186
```




